var LootableItem = (function() {
    var rarity = ["Common", "Uncommon", "Rare", "Epic"],
        weapon = ["Dagger", "1H_Sword", "Wand", "Polearm"],
        armor = ["Helm", "Pauldron", "Chest", "Bracer", "Pants", "Boots"],
        consumable = ["smallHealth", "smallMana", "largeHealth", "largeMana"];

    return {
        
        randomItem: function() {
            return itemArray[Math.floor(Math.random() * itemArray.length)];
        },

        randomRarity: function() {
            return rarity[Math.floor(Math.random() * rarity.length)];
        }
    };
})();

LootableItem.randomItem();
LootableItem.randomRarity();
