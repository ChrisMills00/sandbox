// APPLICATION CONFIGURATION -----------------------------------
// --- require the use of express
var express = require('express');
// --- create instance of express
var app = express();
var port = process.env.PORT || 3000;
// ROUTES ------------------------------------------------------
// You can configure all of your routes here using GET, POST, PUT, 
// PATCH or DELETE methods. You can also send responses.
//      Format: Use instance of Express, attach HTTP method. First argument is
//      the website path, second argument is the function to be executed.

//      It takes 2 arguments: a request and a response (req and res). 
//      REQUEST: The data being requested it fetched.
//      RESPONSE: The data fetched is returned to the web page.
app.get('/', function (req, res) {
  res.send('Hello World!');
});

// SERVER ------------------------------------------------------
var server = app.listen(port, function () {
  console.log('Example app listening at ' + port);
});