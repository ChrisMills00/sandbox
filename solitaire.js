var board = [];
var drawDeck = [];

function Card (type){
    this.type = type;
    this.ace = "Ace";
    this.two = 2;
    this.three = 3;
    this.four = 4;
    this.five = 5;
    this.six = 6;
    this.seven = 7;
    this.eight = 8;
    this.nine = 9;
    this.ten = 10;
    this.jack = "Jack";
    this.queen = "Queen";
    this.king = "King";
}

function makeDeck() {
    var suits = ['spades','diamonds','clubs','hearts'];
    var deck = [];
    for (var i = 0; i < 4; i++) {
        deck.push(new Card(suits[i])
    }
    return deck
}

function shuffle (callback) {
    // randomize all cards into array
    var shuffled = callback;
    // return the deck
    return shuffled.random
}

function dealCards (shuffled) {
    var deck = shuffled()
    // 7 piles
    for (var i = 0; i < 30; i++) {
        // Make a counter x= i+1
        board[i].length = 1 + i
        // Push x cards each round into next pile
        for (var x = 0; x < board[i].length;x++) {
            board[i].push(deck[i])  
        }
    }
    // whats not dealt is the draw deck
    return shuffled.slice(30)
}

// Pile of suits from Ace to King
var orderedSuits = []

// At beginning, only last card face up
function gameStart() {
    // display cards
    
}

// Black/red must alternate
// if diamond, then spade or club
// if spade, then heart or diamond && card < spade
